/*
 * TODO:
 * - Enable user interaction for all type of errors
 */
function WSRL(json){
	this.submitBtnId = json.submitBtnId
	if (!this.submitBtnId){
		throw 'Error: Not button ID in constructor';
	}
	this.apiURL = json.apiURL || 'https://teikirisi.estudiohum.cl/';
	this.siteKey = json.siteKey || 'site-key1';
	this.retryLimit = json.retryLimit || 3;
	this.try = 0;
	this.xhr = null;
	this.timeoutID = null;
	this.debug = json.debug;
	window['WSRL'] = this;
}

WSRL.prototype.Init = function(){
	this.debug && (window.console && console.log('[Init] arguments='+JSON.stringify(arguments)));
	// obtain form
	this.btn = document.getElementById(this.submitBtnId);
	this.frm = this.btn.form;
	// aggregate input hidden field to receive ticket value
	let ipt = document.createElement('input');
	// hidden type
	let t = document.createAttribute('type');
	t.value = 'hidden';
	ipt.setAttributeNode(t)
	// wsrl_ticket name
	let n = document.createAttribute('name');
	n.value = 'wsrl_ticket';
	ipt.setAttributeNode(n)
	this.frm.appendChild(ipt);
	// aggregate input hidden field to receive POT value
	ipt = document.createElement('input');
	// hidden type
	t = document.createAttribute('type');
	t.value = 'hidden';
	ipt.setAttributeNode(t)
	// wsrl_pot name
	n = document.createAttribute('name');
	n.value = 'wsrl_pot';
	ipt.setAttributeNode(n)
	this.frm.appendChild(ipt);

	// set initial invalid status to btn
	this.btn.setCustomValidity('Obtaining access proof of the \'Web Submit Rate Limiter\'. Please wait...')

	// aggregate on-submit form restriction to force pot value
	let instance = this;
	this.btn.addEventListener('click', function(e){
		instance.debug && (window.console && console.log('[on-click] wsrl_ticket.value='+e.target.form.wsrl_ticket.value));
		instance.debug && (window.console && console.log('[on-click] wsrl_pot.value='+e.target.form.wsrl_pot.value));
		if (e.target.form.wsrl_ticket.value === '' || e.target.form.wsrl_pot.value === '') {
			if (instance.timeoutID =! null ) {
				e.target.setCustomValidity('Obtaining access proof of the \'Web Submit Rate Limiter\'. Please wait...')
				instance.debug && (window.console && console.log('[on-click] still waiting'));
			}
			else if (instance.try < instance.retryLimit ) {
				instance.try++;
				e.target.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'. Retrying...')
				instance.getPOT();
				instance.debug && (window.console && console.log('[on-click] fail and retrying'));
			}
			else {
				e.target.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'.\\nPlease retry later reloading the page.')
				instance.debug && (window.console && console.log('[on-click] failed'));
			}
			return false;
		}
		else {
			e.target.setCustomValidity('')
			instance.debug && (window.console && console.log('[on-click] success'));
			return true;
		}
	}, false);
	
	// TODO: Define a timeout for cutting to much large delays advertising user with a message like 'too heavy loaded service'.

	// get POT
	this.getPOT();
}

WSRL.prototype.getPOT = function(){
	this.debug && (window.console && console.log('[getPot] arguments='+JSON.stringify(arguments)));
	let instance = this;
	this.xhr = new XMLHttpRequest();
	this.xhr.addEventListener('load', function(e) {
		instance.debug && (window.console && console.log('[getpot-xhr-load] e.target.readyState='+e.target.readyState));
		instance.debug && (window.console && console.log('[getpot-xhr-load] e.target.status='+e.target.status));
		if (e.target.status == 200) {
			instance.receivePOT();
		}
		else if (instance.try < instance.retryLimit ) {
			instance.try++;
			instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'. Retrying...')
			instance.getPOT();
			instance.debug && (window.console && console.log('[getpot-xhr-load] fail and retrying'));
		}
		else {
			instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'.\\nPlease retry later reloading the page.')
			instance.debug && (window.console && console.log('[getpot-xhr-load] failed'));
		}
	});
	this.xhr.addEventListener('error', function(e) {
		instance.debug && (window.console && console.log('[getpot-xhr-error] e.target.readyState='+e.target.readyState));
		instance.debug && (window.console && console.log('[getpot-xhr-error] e.target.status='+e.target.status));
		instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'.\\nPlease retry later.')
	});
	this.xhr.open('GET', this.apiURL+'api/config', true);
	this.xhr.setRequestHeader('Content-Type', 'application/json');
	this.xhr.setRequestHeader('X-Api-Key', this.siteKey);
	this.xhr.send('');
}

WSRL.prototype.receivePOT = function(){
	this.debug && (window.console && console.log('[receivePOT] arguments='+JSON.stringify(arguments)));
	this.debug && (window.console && console.log('[receivePOT] this.xhr.response='+JSON.stringify(this.xhr.response)));
	let config = JSON.parse(this.xhr.response);
	this.ticket = config.ticket;
	this.delay = config.delay;
	let instance = this;
	this.timeoutID = setTimeout(function(){ instance.sendPOT() }, this.delay);
}

WSRL.prototype.sendPOT = function(){
	let instance = this;
	this.xhr = new XMLHttpRequest();
	this.xhr.addEventListener('load', function(e) {
		instance.debug && (window.console && console.log('[sendpot-xhr-load] e.target.readyState='+e.target.readyState));
		instance.debug && (window.console && console.log('[sendpot-xhr-load] e.target.status='+e.target.status));
		if (e.target.status == 200) {
			instance.setPOT()
		}
		else if (e.target.status == 425) {
			instance.debug && (window.console && console.log('[sendpot-xhr-load] too early, retrying'));
			instance.sendPOT(); // FIXME: race condition over this.timeoutID
		}
		else if (e.target.status == 418) {
			instance.debug && (window.console && console.log('[sendpot-xhr-load] too late, retrying'));
			if (instance.try < instance.retryLimit ) {
				instance.try++;
				instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'. Retrying...')
				instance.getPOT();
				instance.debug && (window.console && console.log('[sendpot-xhr-load] fail and retrying'));
			}
			else {
				instance.debug && (window.console && console.log('[sendpot-xhr-load] failed'));
				instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'.\\nPlease retry later reloading the page.')
			}
		}
	});
	this.xhr.addEventListener('error', function(e) {
		instance.debug && (window.console && console.log('[sendpot-xhr-error] e.target.readyState='+e.target.readyState));
		instance.debug && (window.console && console.log('[sendpot-xhr-error] e.target.status='+e.target.status));
		instance.btn.setCustomValidity('Couldn\'t obtain access proof of the \'Web Submit Rate Limiter\'.\\nPlease retry later.')
	});
	this.xhr.open('POST', this.apiURL+'api/pot', true);
	this.xhr.setRequestHeader('Content-Type', 'application/json');
	this.xhr.setRequestHeader('X-Api-Key', this.siteKey);
	this.xhr.send('{"ticket":"'+this.ticket+'"}');
}

WSRL.prototype.setPOT = function(){
	this.debug && (window.console && console.log('[setPOT] arguments='+JSON.stringify(arguments)));
	this.debug && (window.console && console.log('[setPOT] this.xhr.response='+JSON.stringify(this.xhr.response)));
	let response = JSON.parse(this.xhr.response);
	this.frm.wsrl_ticket.value = this.ticket;
	this.frm.wsrl_pot.value = response.token;
	this.timeoutID = null;
	this.btn.setCustomValidity('')
}
