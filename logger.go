package main

import (
	"encoding/json"

	"go.uber.org/zap"
)

var (
	logger *zap.Logger
)

func initLogger() {
	if logger != nil {
		return
	}
	var cfg zap.Config
	var err error
	if err = json.Unmarshal([]byte(`{
		"level":"debug",
		"encoding": "json",
		"outputPaths": ["/home/teikirisi/app/logs/app.log"],
		"errorOutputPaths": ["stderr", "/home/teikirisi/app/logs/app.log"],
		"encoderConfig": {
			"messageKey": "message",
			"levelKey": "level",
			"nameKey": "logger",
			"timeKey": "timestamp",
			"levelEncoder": "lowercase",
			"timeEncoder": "iso8601"
		}
	}`), &cfg); err != nil {
		panic(err)
	}
	logger = zap.Must(cfg.Build())
	l := logger.Named("initLogger")
	defer l.Sync()
	l.Info("logger construction succeeded")
}
