package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
//	"os"
	"strings"
	"time"

//	"github.com/golang-jwt/jwt"
	"go.uber.org/zap"
)

func getFootPrint(r *http.Request) string {
	// logging
	l := logger.Named("getFootPrint")
	defer l.Sync()

	// client anonymous tracking analysis
	sha := sha256.New()
	// ip
	l.Debug("Remote Address",zap.Any("r.RemoteAddr",r.RemoteAddr))
	sha.Write([]byte(strings.Split(r.RemoteAddr,":")[0]))
	// headers
	l.Debug("Headers",zap.Any("r.Header",r.Header))
	// User-Agent
	sha.Write([]byte(strings.Join(r.Header.Values("User-Agent"),"")))
	footprint := base64.URLEncoding.EncodeToString(sha.Sum(nil))
	l.Debug("footprint",zap.String("f",footprint))
	return footprint
}

func Cleaner() {
	timer := time.Tick(5 * time.Second)
	for {
		select {
			case t := <-timer:
				sessions.Clean(t)
		}
	}
}

func CORS(w http.ResponseWriter, origin string) {
	// CORS
	w.Header().Set("Access-Control-Allow-Origin", origin)
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST")
	w.Header().Set("Access-Control-Max-Age", "1000")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With, X-Api-Key")
}

func Config(w http.ResponseWriter, r *http.Request) {
	// logging
	l := logger.Named("Config")
	defer l.Sync()
	l.Info("begin")

	l.Debug("Request",zap.String("Method",r.Method))
	// Get the expected site-key from headers
	var siteKeyValues = r.Header.Values("X-Api-Key")
	if len(siteKeyValues) == 0 || siteKeyValues[0] == "" {
		if r.Method == "OPTIONS" {
			CORS(w, r.Header.Get("Origin")) // pre-flight
			return
		}
		l.Info("Err: undefined site-key")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Analyze DOS
	footprint := getFootPrint(r)
	l.Debug("clients",zap.Int("Value(footprint)",clients.Value(footprint)))
	if !clients.Inc(footprint) {
		w.WriteHeader(http.StatusTooManyRequests)
		l.Info("Too many requests")
		return
	}

	// Obtaining site key
	var siteKey = siteKeyValues[0]
	// If a site-key NOT exists then return an "NotFound" status
	site, ok := sites.Get(siteKey)
	if !ok {
		l.Info("Err: unexistent site-key")
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// create session
	var s = sessions.Create(footprint, site)
	l.Debug("Session",zap.Any("s",s))
	// JSON obj
	var c struct {
		Ticket string	`json:"ticket"`
		Delay  int64	`json:"delay"`
	}
	// fill JSON obj
	c.Ticket = s.ticket
	c.Delay = s.delay.Milliseconds()
	// create JSON string
	b, _ := json.Marshal(c)
	// response
	CORS(w, site.getOrigin().String())
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache,no-store,must-revalidate,proxy-revalidate")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(b))
	w.Write([]byte("\n"))
	return
}

func Pot(w http.ResponseWriter, r *http.Request) {
	// logging
	l := logger.Named("Pot")
	defer l.Sync()
	l.Info("begin")

	l.Debug("Request",zap.String("Method",r.Method))
	// Get the expected site-key from headers
	var siteKeyValues = r.Header.Values("X-Api-Key")
	if len(siteKeyValues) == 0 || siteKeyValues[0] == "" {
		if r.Method == "OPTIONS" {
			CORS(w, r.Header.Get("Origin")) // well-intentioned pre-flight
			return
		}
		l.Info("Err: undefined site-key")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Analyze DOS
	footprint := getFootPrint(r)
	if !clients.Inc(footprint) { // FIXME: this prohibition could affect a well-intentioned customer who has already waited, disrupting their legitimate flow.
		w.WriteHeader(http.StatusTooManyRequests)
		l.Info("Too many requests")
		return
	}

	// Obtaining site key
	var siteKey = siteKeyValues[0]
	// If a site-key NOT exists then return an "NotFound" status
	site, ok := sites.Get(siteKey)
	if !ok {
		l.Info("Err: unexistent site-key")
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// response headers
	CORS(w, site.getOrigin().String())
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache,no-store,must-revalidate,proxy-revalidate")

	var input struct {
		Ticket string	`json:"ticket"`
	}
	// Get the JSON body and decode into input
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		l.Info("Err: wrong body")
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	l.Info("received ticket",zap.String("input.Ticket",input.Ticket))
	// Get the expected session from our in memory map
	s, ok := tickets.Get(input.Ticket)
	// if the ticket NOT exists, then we return a "NotFound" status
	if !ok {
		l.Info("Err: unexistent ticket")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	l.Debug("Session",zap.Any("s",s))
	// if the session is verified already, "BadRequest"
	if s.isVerified() {
		l.Info("Err: already verified proof")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	l.Info("valid ticket")
	var _now = time.Now()
	l.Info("actual time",zap.Time("_now",_now))
	if s.timestamp.Add(s.delay).After(_now) {
		l.Info("too early")
		w.WriteHeader(http.StatusTooEarly)
		return
	} else if s.timestamp.Add(2*s.delay).Before(_now) {
		l.Info("too late")
		w.WriteHeader(http.StatusTeapot)
		return
	}
	l.Info("in time")
	// mark session verified
	s.setVerified()

	// generate TOTP
	validity := site.getValidity()
	var data = []byte(fmt.Sprintf("%x",int64(_now.Unix()/int64(math.Round(validity.Seconds())))))
	data = append(data,conf.serverKey...)
	data = append(data,[]byte(fmt.Sprintf("%x",input.Ticket))...)
	l.Info("totp data",zap.String("data",fmt.Sprintf("%x",data)))
	var t struct {
		Token string `json:"token"`
	}
	t.Token = fmt.Sprintf("%x",sha1.Sum(data))
	b, _ := json.Marshal(t)
	// response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(b))
	w.Write([]byte("\n"))
	return
}

func Validate(w http.ResponseWriter, r *http.Request) {
	l := logger.Named("Validate")
	defer l.Sync()
	l.Info("begin")

	l.Debug("Request",zap.String("Method",r.Method))
	// Get the expected site-key from headers
	var siteKeyValues = r.Header.Values("X-Api-Key")
	if len(siteKeyValues) == 0 || siteKeyValues[0] == "" {
		if r.Method == "OPTIONS" {
			CORS(w, r.Header.Get("Origin")) // well-intentioned pre-flight
			return
		}
		l.Info("Err: undefined site-key")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var siteKey = siteKeyValues[0]
	// If a site-key NOT exists then return an "NotFound" status
	site, ok := sites.Get(siteKey)
	if !ok {
		l.Info("Err: unexistent site-key")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	// Get the expected site-secret from headers
	var siteAuthValues = r.Header.Values("Authorization")
	if len(siteAuthValues) == 0 || siteAuthValues[0] == "" {
		l.Info("Err: undefined site-secret")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var siteSecret = strings.Trim(strings.TrimPrefix(siteAuthValues[0],"Bearer")," ")
	// validate site auth
	if !site.Auth(siteSecret) {
		l.Info("Err: wrong site-secret")
		// Analyze DOS
		footprint := getFootPrint(r)
		if !clients.Inc(footprint) {
			w.WriteHeader(http.StatusTooManyRequests)
			return
		}
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// read body
	body, err := io.ReadAll(r.Body)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		l.Info("Err: wrong body")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	l.Debug("received body",zap.Any("body",body))
	var input struct {
		Token string `json:"token"`
		Ticket string `json:"ticket"`
	}
	// Get the JSON body and decode into input
	err = json.NewDecoder(strings.NewReader(string(body))).Decode(&input)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		l.Info("Err: wrong body",zap.Error(err))
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	l.Debug("received ticket",zap.String("input.Ticket",input.Ticket))
	l.Debug("received token",zap.String("input.Token",input.Token))
	// Get the expected session from our in memory map
	s, ok := tickets.Get(input.Ticket)
	// If the ticket NOT exists, then we return a "BadRequest" status
	if !ok {
		l.Info("Err: unexistent ticket")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	l.Debug("Session",zap.Any("s",s))
	// if the session is verified already, "BadRequest"
	if !s.isVerified() {
		l.Info("Err: not verified proof")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validity
	var _now = time.Now()
	l.Info("actual time",zap.Time("_now",_now))
	// generate actual TOTP
	validity := site.getValidity()
	var segment = int64(_now.Unix()/int64(validity.Seconds()))
	var data = []byte(fmt.Sprintf("%x", segment))
	data = append(data,conf.serverKey...)
	data = append(data,[]byte(fmt.Sprintf("%x",input.Ticket))...)
	l.Info("token data",zap.String("data",fmt.Sprintf("%x",data)))
	var nowToken = fmt.Sprintf("%x",sha1.Sum(data))
	l.Info("actual Token",zap.String("nowToken",nowToken))
	// JSON obj
	var c struct {
		Proof bool	`json:"proof"`
	}
	if nowToken != input.Token {
		// Note: near changing segment time point, the validity time is tiny.
		// So, in case of failing validation, try validate against previous
		// validity segment
		data = []byte(fmt.Sprintf("%x", segment - 1))
		data = append(data,conf.serverKey...)
		data = append(data,[]byte(fmt.Sprintf("%x",input.Ticket))...)
		l.Info("token data",zap.String("data",fmt.Sprintf("%x",data)))
		var prevToken = fmt.Sprintf("%x",sha1.Sum(data))
		l.Info("previous Token",zap.String("prevToken",prevToken))
		if prevToken != input.Token {
			c.Proof = false
			l.Info("Proof expired")
		} else {
			c.Proof = true
			l.Info("Validated!:")
			// remove ticket from in memory storage
			tickets.Unset(input.Ticket)
		}
	} else {
		c.Proof = true
		l.Info("Validated!:")
		// remove ticket from in memory storage
		tickets.Unset(input.Ticket)
	}
	// create JSON string
	b, err := json.Marshal(c)
	// response
	CORS(w, site.getOrigin().String())
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache,no-store,must-revalidate,proxy-revalidate")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(b))
	w.Write([]byte("\n"))
	return
}
