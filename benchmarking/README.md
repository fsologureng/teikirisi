Projected list of K6 benchmarking tests:

1) Whole WRSL cycle with a browser against a working instance.
 - script from 1 IP with xk6-browser.
 - metrics:
   - http status
   - API driven delay

2) Fast submit against a protected service by k6 browser.
 - script from 1 IP with xk6-browser.
 - metric:
   - http status
   - request duration

3) Distributed config requests against a test mirror 
 - script from N IPs with k6 over VM with a private network with multiple NICs.
 - script from 1 IP with xk6-browser with whole wsrl cycle. 
 - requirements:
   - 1 vm for k6 (multiple NICs with one-to-one routing)
   - 1 vm for teikirisi
   - 1 vm for xk6-browser
   - modify teikirisi to allow plain http
 - metrics:
   - API driven delay
   - request duration

4) Whole WSRL cycle with distinct browsers (UA header) from single IP against a working instance. 
 - script with UA by VU with xk6-browser.
 - metric:
   - http status
   - API driven delay

5) POST submit to validate point
 - script from 1 IP with k6 (before banning).
 - metric:
   - http status
   - request duration
