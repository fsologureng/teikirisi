//TODO: import http from "k6/http";
import { check, sleep } from 'k6';
import { Counter, Rate, Trend } from "k6/metrics";
import { chromium } from 'k6/x/browser';

export const options = {
	vus: 1,
 	duration: '5m',
}

// Custom metrics
// We instantiate them before our main function
let Submits = new Counter("submission counter");
let timeToFirstByte = new Trend("time_to_first_byte", true);
let delayToWait = new Trend("delay_to_wait", false);

export default function () {
	const browser = chromium.launch({
		args: ['aggressive-cache-discard','allow-profiles-outside-user-dir'],
		headless: true,
		slowMo: '500ms',
		timeout: '5m',
		debug: false
	});

	const context = browser.newContext();
	const page = context.newPage();
	page.setDefaultNavigationTimeout(300000);
	page.setDefaultTimeout(300000);

	let nav = Promise.any([
		Promise.resolve(page.goto('https://www.estudiohum.cl/~felipe/teikirisi-test/', { waitUntil: 'domcontentloaded' })).then(()=>{
			console.log('textarea='+page.inputValue('textarea'));
			return Promise.resolve(page.type('textarea', 'Hola mundo xk6-browser',{}))
		}).then(()=>{
			console.log("[page.type(textearea)] done");
			// page.screenshot({ path: `example-chromium-fill.png` });
			return Promise.resolve(page.click('#theBtn',{}))
		},()=>{
			console.log("[page.type(textearea)] fail");
			return Promise.resolve(false);
		}).then(()=>{
			console.log("[page.click (1st)] done");
			// page.screenshot({ path: `example-chromium-invalid.png` });
			return Promise.resolve(page.waitForFunction(()=>{
				if ( window.theBtn.validity.valid ){
					console.log("[page.waitForFunction(validity)] is valid");
					return true;
				}
				return null;
			}, {timeout: 300000} ).then(() => {
				console.log("[page.waitForFunction(validity).then] done");
				return Promise.resolve(page.waitForFunction("window.WSRL.hasOwnProperty('delay')", {timeout: 30} )).then(() => {
						console.log("[page.waitForFunction(delay).then] done");
						return Promise.resolve(page.evaluate(() => window.WSRL.delay));
					}, (err) => {
						console.log("[page.waitForFunction (delay)] fail: "+err);
						return Promise.resolve(false);
					}).then((delay) => {
						console.log("[page.evaluate(delay).then] done delay="+delay);
						delayToWait.add(delay);
						// page.screenshot({ path: `example-chromium-click.png` });
						return Promise.all([
							page.waitForNavigation({waitUntil:'domcontentloaded'}),
							page.click('#theBtn',{}),
						])
					}, (err) => {
						console.log("[page.evaluate(delay)] fail: "+err);
						return Promise.resolve(false);
					}).then((val) => {
						console.log("[page.waitForNavigation.then] done");
						if (val) {
							Submits.add(1);
							console.log("[page.waitForNavigation.then] response: "+val[0].status());
							if ( val[0].status() == 200 ) {
					    		check(page, { 'status_ok': true });
							}
							else if ( val[0].status() == 403 ) {
					    		check(page, { 'status_forbidden': true });
							}
							//TODO: with http pkg: timeToFirstByte.add(val[0].timings.waiting, { ttfbURL: val[0].url });
						}
						return Promise.resolve(false);
						// page.screenshot({ path: `example-chromium-submit.png` });
					}, (val) => {
						console.log("[page.waitForNavigation.then] fail: "+val);
						return Promise.resolve(false);
					});
				}, (err) => {
					console.log("[page.waitForFunction(validity).then] fail: "+err);
					return Promise.resolve(false);
				}));
			},()=>{
				console.log("[page.click (1st)] fail");
				return Promise.resolve(false);
			}),
		page.waitForFunction(()=>{
			if ( window.WSRL.xhr.readyState == 4 && window.WSRL.xhr.status == 0 ) {
				console.log("[page.waitForFunction(xhr)] ajax error");
				return true;
			}
			return null;
		}, {timeout: 300000}).then(()=>{ return Promise.resolve(true) })
	]).then((any) => {
		console.log("[Promise.Any] any="+any);
		if (any) {
			check(page, { 'status_forbidden': true });
		}
		page.close();
		browser.close();
	});
}

