package main

import (
	"errors"
	"fmt"
	"math"
	"net/url"
	"sync"
	"time"

	"go.uber.org/zap"
)

var (
	// PoC sites
	sites = siteIndex{
		i: map[string]*site{ // registry of sites TODO: persistence store of site config
			"site-key1": &site{
				siteKey: "site-key1",
				siteSecret: "site-pass1",
				gW: 0.03,
				fW: 0.4,
				baseDelay: func() time.Duration {
					d, _ := time.ParseDuration("2000ms")
					return d
				}(),
				validity: func() time.Duration {
					d, _ := time.ParseDuration("15m")
					return d
				}(),
				origin: func() url.URL {
					o, _ := url.Parse("https://www.estudiohum.cl")
					return *o
				}(),
			},
			"site-key2": &site{
				siteKey: "site-key2",
				siteSecret: "site-pass2",
				gW: 0.04,
				fW: 0.6,
				baseDelay: func() time.Duration {
					d, _ := time.ParseDuration("2500ms")
					return d
				}(),
				validity: func() time.Duration {
					d, _ := time.ParseDuration("3h")
					return d
				}(),
				origin: func() url.URL {
					o, _ := url.Parse("https://www.estudiohum.cl")
					return *o
				}(),
			},
			"site-key3": &site{
				siteKey: "site-key3",
				siteSecret: "site-pass3",
				gW: 0.02,
				fW: 0.7,
				baseDelay: func() time.Duration {
					d, _ := time.ParseDuration("1500ms")
					return d
				}(),
				validity: func() time.Duration {
					d, _ := time.ParseDuration("30m")
					return d
				}(),
				origin: func() url.URL {
					o, _ := url.Parse("https://www.estudiohum.cl")
					return *o
				}(),
			},
			"swagger-site-key": &site{
				siteKey: "swagger-site-key",
				siteSecret: "swagger-site-pass",
				gW: 0.02,
				fW: 0.7,
				baseDelay: func() time.Duration {
					d, _ := time.ParseDuration("3000ms")
					return d
				}(),
				validity: func() time.Duration {
					d, _ := time.ParseDuration("30m")
					return d
				}(),
				origin: func() url.URL {
					o, _ := url.Parse("https://inspector.swagger.io")
					return *o
				}(),
			},
		},
	}

	lastTicket = int(0) // last ticket emmitted
	// FIXME: can't be a serial sequence
	// TODO: if ticket is a TOTP then expires by itself and allows to clean queues without much cost but must be unique, maybe JWT?
	// Memory Storage
	tickets = SessionIndex{i: make(map[string]*session)} // structure to store sessions by ticket
	clients = ClientCounter{v: make(map[string]int)} // counter of sessions by footprint
	// TODO: send config through JWT to client to avoid local persistance
	// TODO: BUT a client-footprint/ticket queue is needed to avoid DOS attacks
	// TODO: add a task to clean up expired issued proofs
	sessions = SessionManager{}
)

/**
 * Structure to store info site that represents a protected wed resource.
 * It provides concurrency control
 */
type site struct {
	mu		sync.Mutex
	siteKey		string
	siteSecret	string
	gW		float64 // tickets weight (global load)
	fW		float64 // footprint weight (individual load)
	baseDelay	time.Duration // proof of time duration
	validity	time.Duration // validation time long window of TOTP password
	origin		url.URL // URL of protected service
}

/**
 * Function to validate secret shared with protected service to control validation access
 */
func (s *site) Auth(secret string) bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.siteSecret == secret
}

/**
 * Function to obtain Origin asociated with protected service to response CORS headers
 */
func (s *site) getOrigin() *url.URL {
	s.mu.Lock()
	defer s.mu.Unlock()
	return &s.origin
}

/**
 * Method to obtain the amount of time during which the POT is valid, configuration site-dependant
 */
func (s *site) getValidity() time.Duration {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.validity
}

/**
 * Method to obtain the minimum amount of time during the client must wait to access the protected resource.
 * The delay obtained is an exponential function with a base delay and a slope defined by current active tickets number, current active footprints number, a tickets weight and a footprint weight
 */
func (s *site) getDelay(n_connections, n_tickets int) time.Duration {
	s.mu.Lock()
	defer s.mu.Unlock()
	return time.Duration(int64(s.baseDelay)*int64(math.Round(math.Exp(s.gW*float64(n_tickets)+s.fW*float64(n_connections)))))
}

/**
 * Structure to store sites by key with concurrently control
 */
type siteIndex struct {
	mu sync.Mutex
	i map[string]*site
}

/**
 * Method to obtain a site by its key from the index
 */
func (s *siteIndex) Get(key string) (*site, bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	si, ok := s.i[key]
	return si, ok
}

/**
 * Structure to store session info.
 * Each session stores information about a web user of a protected site who is intenting to pass a POT in order to be allowed to make a submission.
 */
type session struct {
	// inmutables
	footprint	string // client anonymous identifier
	siteKey		string // Site-Key associated with the session
	ticket		string // Ticket of the session
	delay			time.Duration // Delay assigned to the client
	timestamp	time.Time  // Creation timestamp

	// mutables
	mu				sync.Mutex
	verified		bool	// state of validation of the POT
	next			*session // link to the next (least newer) session
}

/**
 * Method to obtain session's verifiable state
 */
 func (s *session) isVerified() bool {
	 s.mu.Lock()
	 defer s.mu.Unlock()
	 return s.verified
 }

/**
 * Method to set session verified
 */
 func (s *session) setVerified() {
	 s.mu.Lock()
	 defer s.mu.Unlock()
	 s.verified = true
 }

/**
 * implement Stringer
 */
 func (s *session) String() string {
	 return fmt.Sprintf("{footprint:%s, siteKey:%s, ticket:%v, delay:%v, timestamp:%v, verified:%v}", s.footprint, s.siteKey, s.ticket, s.delay, s.timestamp.Unix(), s.verified)
}
/**
 * Structure for produce sessions ordered by time in a linked list
 */
type SessionManager struct {
	mu sync.Mutex
	oldest *session // the most older session stored
	last *session // the last session created
}

/**
 * Method to create a session with a footprint in a site context
 */
func (S *SessionManager) Create(footprint string, site *site) *session {
	l := logger.Named("SessionManager.Create")
	l.Debug("begin");

	// get ticket
	S.mu.Lock()
	lastTicket+=1
	ticket := lastTicket
	S.mu.Unlock()

	// FIXME: There must be a balance between the rate per IP and the rate per UserAgent (UA).
	// Currently, changing IP is as easy as changing UA in the eyes of the limiter.
	// However, it is much easier to subvert the UserAgent to gain advantage for an attack
	// obtaining different footprints. So a high rate for the same IP for such an attack is
	// against well-intentioned UA users.
	// But at the same time, many well-intentioned users who share the same UA for reasons of
	// anonymity or massiveness will have higher rates, regardless of their IPs.
	delay := site.getDelay(clients.Value(footprint), clients.Length())
	l.Debug("delay",zap.Duration("d",delay));
	// create session
	s := &session{
		footprint: footprint,
		siteKey: site.siteKey,
		ticket: fmt.Sprintf("%v",ticket),
		delay: delay,
		timestamp: time.Now(),
		verified: false,
	}
	// link new session to the list
	S.mu.Lock()
	if S.oldest == nil {
		S.oldest = s
	}
	if S.last != nil {
		S.last.next = s
	}
	S.last = s
	S.mu.Unlock()

	// store local session obj
	tickets.Set(s.ticket, s)
	l.Debug("end");
	return s
}

func (S *SessionManager) Clean(now time.Time) error {
	l := logger.Named("SessionManager.Clean")
//	l.Debug("begin");

	S.mu.Lock()
	// cleaning obsolete sessions following the linked list in reverse order removing oldest sessions
	for i := 0; i < 10; i++ { // limit cleaning to a number of loops by call to avoid prolongued locks
		if S.oldest != nil {
			site, ok := sites.Get(S.oldest.siteKey)
			if !ok {
				return errors.New(fmt.Sprintf("Unexistent site: %v", S.oldest.siteKey))
			}
			validity := site.getValidity()
			// Note: near changing segment time point, the validity time is tiny,
			// so the validation consider two segments.
			if S.oldest.timestamp.Add(2*validity).Before(now) {
				obsolete := S.oldest
				l.Debug("Clean oldest", zap.Any("obsolete",obsolete));
				// cleaning tickets
				tickets.Unset(obsolete.ticket)
				// reduce footprint record
				// FIXME: there is no matching between Inc and Dec counting,
				// leading to maintain some permanently banned footprints
				clients.Dec(obsolete.footprint)
				// de-list obsolete session
				S.oldest = obsolete.next
				obsolete.next = nil
			} else {
				l.Debug("Oldest no obsolete yet");
				break
			}
		} else {
			l.Debug("No sessions");
			break
		}
	}
	S.mu.Unlock()
//	l.Debug("end");
	return nil
}

/**
 * Structure to store sessions by footprint concurrently
 */
type SessionIndex struct {
	mu sync.Mutex
	i map[string]*session
}

func (f *SessionIndex) Set(key string, s *session) {
	f.mu.Lock()
	f.i[key] = s
	f.mu.Unlock()
}

func (f *SessionIndex) Unset(key string) {
	f.mu.Lock()
	delete(f.i, key)
	f.mu.Unlock()
}

func (f *SessionIndex) Get(key string) (*session, bool) {
	f.mu.Lock()
	defer f.mu.Unlock()
	s, err := f.i[key]
	return s, err
}

func (f *SessionIndex) Length() int {
	f.mu.Lock()
	defer f.mu.Unlock()
	return len(f.i)
}

/**
 * Structure to store a counter by footprint concurrently
 */
type ClientCounter struct {
	mu sync.Mutex
	v map[string]int
}

// Inc increments the counter of the given footprint.
func (c *ClientCounter) Inc(footprint string) bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.v[footprint] == conf.limiter.maxRequests {
		return false
	}
	c.v[footprint]++
	return true
}

// Dec decrements the counter of the given footprint.
func (c *ClientCounter) Dec(footprint string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if _, ok := c.v[footprint]; !ok {
		return
	}
	c.v[footprint]--
	if c.v[footprint] == 0 {
		delete(c.v, footprint)
	}
	return
}

// Value returns the current value of the counter for the given footprint.
func (c *ClientCounter) Value(footprint string) int {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.v[footprint]
}

// returns how many different footprints are registered
func (c *ClientCounter) Length() int {
	c.mu.Lock()
	defer c.mu.Unlock()
	return len(c.v)
}

