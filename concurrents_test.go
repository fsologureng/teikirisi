package main

import (
	"fmt"
	"testing"
)

func init() {
	conf.serverKey = "my_server_key"
	conf.limiter.maxRequests = 10
}

func TestSessionIndex(t *testing.T) {
	idx := SessionIndex{i: make(map[string]*session)}
	ch := make(chan bool)
	// test concurrent access
	for i := 0; i < 1000; i++ {
		go func() {
			idx.Set("somekey1", &session{ ticket: fmt.Sprintf("ticket_%v",i)})
			ch <- true
		}()
	}
	for i := 0; i < 1000; i++ {
		<- ch
	}
	s, ok := idx.Get("somekey1")
	if !ok {
		t.Fatalf(`Value() must return ok if the key exists, %v`, ok)
	} else if s.ticket != "ticket_1000" {
		t.Fatalf(`Value() implement non-concurrent access, %v`, s)
	}
	// test defined value
	s, ok = idx.Get("somekey2")
	if ok {
		t.Fatalf(`Value() must return not ok if the key doesn't exist, %v`, ok)
	} else if s != nil {
		t.Fatalf(`Value() must return undefined if the key doesn't exist, %v`, s)
	}
	idx.Set("somekey2", &session{ ticket: "ticket_1"})
	s, ok = idx.Get("somekey2")
	if !ok {
		t.Fatalf(`Value() must return ok if the key exists, %v`, ok)
	} else if s.ticket != "ticket_1" {
		t.Fatalf(`Value() must return the stored value, %v`, s)
	}
	idx.Unset("somekey2")
	s, ok = idx.Get("somekey2")
	if ok {
		t.Fatalf(`Value() must return not ok if the key doesn't exist, %v`, ok)
	} else if s != nil {
		t.Fatalf(`Value() must return undefined if the key doesn't exist, %v`, s)
	}
}

func TestClientCounter(t *testing.T) {
	c := ClientCounter{v: make(map[string]int)}
	ch := make(chan bool)
	// test concurrent access
	for i := 0; i < 1000; i++ {
		go func() {
			ch <- c.Inc("somefootprint")
		}()
	}
	for i := 0; i < 1000; i++ {
		<- ch
	}
	if c.Value("somefootprint") != conf.limiter.maxRequests {
		t.Fatalf(`Inc() implement non-concurrent access, %v`, c.Value("somefootprint"))
	}
	for i := 0; i < 1000; i++ {
		go func() {
			c.Dec("somefootprint")
			ch <- true
		}()
	}
	for i := 0; i < 1000; i++ {
		<- ch
	}
	if c.Value("somefootprint") != 0 {
		t.Fatalf(`Dec() implement non-concurrent access, %v`, c.Value("somefootprint"))
	}
}
