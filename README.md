# README

Work-in-progress (not ready for use).

## Introduction

*Teikirisi* is intended to be a web form submit adaptive rate limiter.

*Teikirisi* can limit the submission rate of a web form by forcing the browser to wait a convenient amount of time, called Proof of Time (PoT), between receiving the form and submitting. In this way, the protected service only receives submission requests at a maximum amount per unit of time by user. 

## How it works

Similar to a CaptCHA, *Teikirisi* instructs the user's browser to wait a certain amount of time as a test. Once that time has elapsed, it will provide a token to send along with the form data to the protected service, which will validate the token against *Teikirisi* and, based on the response, allow or deny access to the resource.  
While the browser waits, the submit button is marked as invalid so that the form cannot be submitted. This behaviour does not require user interaction unless that button is pressed, so the user is alerted via a standard browser message created via the Web API, until the PoT has passed.

If a client intends to submit multiple forms at a high rate, *Teikirisi* exponentially increases the wait time to pass the PoT, reducing the efficiency of the form submission. To pseudo-identify mutiple requests from the same IP, *Teikirisi* takes a footprint of the client's IP via a perfect hash function and stores it in volatile memory. In addition, it does the same with the client's User-Agent to pseudo-differentiate clients connected from the same IP.  
Other footprints obtained by session cookies issued in conjunction with a temporary redirection are under study.

In case of distributed attempts to submit forms concurrently, *Teikirisi* increase exponentially but softly the time needed to pass the PoT as a sign of overload, discouraging DDOS attacks, but allowing well behaving submits.

All time increment rates are configurable per site.

## What teikirisi does
 * Teikirisi limits the rate of submitting a protected form forcing the browser to wait a defined amount of time, denominated Proof of Time (PoT).
 * Teikirisi is privacy friendly as not stores sensitive user data, using ethical techniques to use that information and not using persistent storage for it.

## What teikirisi does not
 * __Teikirisi is not a CaptCHA__; Teikirisi can not detect if a web form visitor is a human or is a robot.
 * __Teikirisi is not a general purpose rate limiter__; Teikirisi works at a high level of server client negotiation; Deeper protection needs low level applications.
 * *Teikirisi* does __not stores sensitive user data__, using ethical techniques for the use of that information.

## Acknowledgements
 * [Aravinth Manivannan](https://codeberg.org/realaravinth) for his ideas in [mCaptcha](https://github.com/mCaptcha). 
 * [Codeberg](https://www.codeberg.org) for their willingness to solve [this](https://codeberg.org/Codeberg/Community/issues/479) [problem](https://mastodon.social/web/@humanetech/108673514211178268) and for encourage me to learn Golang and to make a PoC of this.
