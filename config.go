package main

import (
	"errors"
	"fmt"
	"net/mail"

	"gopkg.in/ini.v1"
)

var (
	conf config
)

type config struct {
	serverKey string // server instance private key
	limiter struct {
		maxRequests int // Number of request allowed before being banned
	}
	acme struct {
		email string
	}
}

func init() {
	cfg, err := ini.Load("conf/app.ini")
	if err != nil {
		fmt.Printf("Fail to read ini file: %v\n", err)
	} else {
		// serverKey
		fmt.Println("App Mode:", cfg.Section("").Key("APP_MODE").String())
		conf.serverKey = cfg.Section("").Key("SERVER_KEY").MustString("")
		if len(conf.serverKey) == 0 {
			panic(errors.New("SERVER_KEY undefined"))
		}

		// limiter
		conf.limiter.maxRequests = cfg.Section("limiter").Key("MAX_REQUESTS").MustInt(10)
		fmt.Println("Max requests to banning: ", conf.limiter.maxRequests)

		// acme
		conf.acme.email = cfg.Section("acme").Key("EMAIL").Validate(func (in string) string {
			a, err := mail.ParseAddress(in)
			if err != nil {
				return ""
			} else if in == a.Address {
				return in
			} else {
				return ""
			}
		})
		fmt.Println("Email for ACME requests: ", conf.acme.email)
	}
}
