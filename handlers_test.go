package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)
var (
	test_sitekey = string("site-key1")
	test_sitesecret = string("site-pass1")
	test_ticket string
	test_delay time.Duration
	test_token string
)

func init() {
	conf.serverKey = "my_server_key"
	conf.limiter.maxRequests = 10
}

func TestConfigHandler(t *testing.T) {
	initLogger()
	ts := httptest.NewServer(http.HandlerFunc(Config))
	defer ts.Close()

	client := &http.Client{}
	req, _ := http.NewRequest("GET", ts.URL, nil )
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Api-Key", test_sitekey)
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf(`Config(), %v, want OK response with valid site key, nil`, err)
	}
	// JSON input
	var res struct {
		Ticket string	`json:"ticket"`
		Delay  int64	`json:"delay"`
	}
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		t.Fatalf(`Config(), %v, want json response, nil`, err)
	}
	resp.Body.Close()

	if res.Ticket == "" {
		t.Fatalf(`Config() want a non-empty ticket, nil`)
	} else{
		test_ticket = res.Ticket
	}
	if res.Delay == 0 {
		t.Fatalf(`Config(), want a non-zero delay, nil`)
	} else{
		test_delay = time.Duration(res.Delay)*time.Millisecond
	}
}

func TestPotHandler(t *testing.T) {
	initLogger()
	ts := httptest.NewServer(http.HandlerFunc(Pot))
	defer ts.Close()

	// JSON output structure
	var pd struct {
		Ticket string `json:"ticket"`
	}
	// valid site key
	pd.Ticket = test_ticket
	// create JSON string
	b, err := json.Marshal(pd)
	// create http client
	client := &http.Client{}
	req, _ := http.NewRequest("POST", ts.URL, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Api-Key", test_sitekey)
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf(`Pot(), %v, want HTTP response with valid ticket, nil`, err)
	} else if resp.StatusCode != 425 {
		t.Fatalf(`Pot(), %v, want 'Too early' response with valid ticket, nil`, resp.Status)
	}
	time.Sleep(test_delay)
	req, _ = http.NewRequest("POST", ts.URL, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Api-Key", test_sitekey)
	resp, err = client.Do(req)
	if err != nil {
		t.Fatalf(`Pot(), %v, want HTTP response with valid ticket, nil`, err)
	} else if resp.StatusCode != 200 {
		t.Fatalf(`Pot(), %v, want OK response with valid ticket, nil`, resp.Status)
	}

	// JSON input
	var res struct {
		Token string	`json:"token"`
	}
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		t.Fatalf(`Pot(), %v, want json response, nil`, err)
	}
	resp.Body.Close()

	if res.Token == "" {
		t.Fatalf(`Config() want a non-empty token, nil`)
	} else {
		test_token = res.Token
	}
}

func TestValidateHandler(t *testing.T) {
	initLogger()
	ts := httptest.NewServer(http.HandlerFunc(Validate))
	defer ts.Close()

	// JSON output structure
	var pd struct {
		Ticket string `json:"ticket"`
		Token string `json:"token"`
	}
	// valid site key
	pd.Ticket = test_ticket
	pd.Token = test_token
	// create JSON string
	b, err := json.Marshal(pd)
	// create http client
	client := &http.Client{}
	req, _ := http.NewRequest("POST", ts.URL, bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Api-Key", test_sitekey)
	req.Header.Set("Authorization", "Bearer "+test_sitesecret)
	resp, err := client.Do(req)
	if err != nil {
		t.Fatalf(`Validate(), %v, want HTTP response with valid token, nil`, err)
	} else if resp.StatusCode != 200 {
		t.Fatalf(`Validate(), %v, want OK response with valid token, nil`, resp.Status)
	}

	// JSON input
	var res struct {
		Proof bool	`json:"proof"`
	}
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		t.Fatalf(`Validate(), %v, want json response, nil`, err)
	}
	resp.Body.Close()

	if !res.Proof {
		t.Fatalf(`Validate() want a valid proof, nil`)
	}
}
