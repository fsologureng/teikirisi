package main

import (
	"net/http"

	"github.com/caddyserver/certmagic"
	"go.uber.org/zap"
)

func main() {
	// init logger
	initLogger()
	l := logger.Named("main")
	defer l.Sync()

	// certmagic config
	// read and agree to your CA's legal documents
	certmagic.DefaultACME.Agreed = true
	// provide an email address
	certmagic.DefaultACME.Email = conf.acme.email
	// use the staging endpoint while we're developing
	certmagic.DefaultACME.CA = certmagic.LetsEncryptProductionCA

	// cleaner
	go Cleaner()

	// http api handlers
	mux := http.NewServeMux()
	mux.HandleFunc("/api/config", Config)
	mux.HandleFunc("/api/pot", Pot)
	mux.HandleFunc("/api/validate", Validate)

	// start the server on port 443 with redirect from 80
	err := certmagic.HTTPS([]string{"teikirisi.estudiohum.cl"}, mux)
	if err != nil {
		l.Fatal("Fatal err", zap.Error(err))
	}
}
